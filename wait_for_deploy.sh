#!/bin/sh
deploystart=$(date +%s)
timeout=900 # Seconds to wait before error
threshhold=$((deploystart + timeout))
echo "Waiting for update to version $3"
while true; do
    # Check for timeout
    timenow=$(date +%s)
    if [ "$timenow" -gt "$threshhold" ]; then
        echo "Timeout - $timeout seconds elapsed"
        exit 1
    fi

    # See what's deployed
    version=`aws elasticbeanstalk describe-environments --application-name $1 --environment-name $2 --query "Environments[*].VersionLabel" --output text`
    status=`aws elasticbeanstalk describe-environments --application-name $1 --environment-name $2 --query "Environments[*].Status" --output text`

    if [ "$version" != "$3" ]; then
        echo "Tag not updated (currently $version). Waiting."
        sleep 10
        continue
    fi
    if [ "$status" != "Ready" ]; then
        echo "System not Ready -it's $status. Waiting."
        sleep 10
        continue
    fi
    break
done
