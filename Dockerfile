from alpine:latest

RUN apk add --no-cache git bash curl build-base \
    zlib-dev \
    libffi-dev \ 
    bzip2-dev \
    libressl-dev \
    expat-dev
RUN git clone https://github.com/aws/aws-elastic-beanstalk-cli-setup.git
RUN ./aws-elastic-beanstalk-cli-setup/scripts/bundled_installer
RUN ln -s ~/.pyenv/versions/3.7.2/bin/* /usr/local/bin/ && ln -s ~/.ebcli-virtual-env/executables/* /usr/local/bin/
RUN pip install awscli --upgrade --user && ln -s ~/.local/bin/* /usr/local/bin/
ADD wait_for_deploy.sh .
CMD aws
